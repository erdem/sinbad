string dizineCevir (string kutukIsmi)
{
    string dizin = kutukIsmi.split(".")[0];
    dizin = dizin.split("_")[0];
    dizin = dizin.replace(" ", "_");
    return dizin;
}

bool arapcaMi (string metin)
{
  string alfabe = "ابتثجحخدذرزسشصضعغفقكلمنوهي";
  for (int i=0; i < metin.char_count(); i++)
  {
	string karakter =
	metin.get_char(metin.index_of_nth_char(i)).to_string();
	if (alfabe.contains(karakter))
	{
	  return true;
	}
  }
  return false;
}


// string diliBelirle(string kelime)
// {
//     string sonuc;
//     string uygulamaDizini = "/home/erdem/Belgeler/sinbad/";
// 	string kutuk = uygulamaDizini + "dilbul.py";

// 	Process.spawn_command_line_sync("python " +
// 	  kutuk + " " + kelime, out sonuc);

//     if (sonuc.strip() == "ar" ||
//        sonuc.strip() == "fa" || sonuc.strip() == "ur" )
//     {
//          return "ara";
//     }
//     else
//     {
//         return "tur";
//     }
// }



void anaKutuguEkle(string kelime, string gecerliDizin,
				   string gecerliKutuk, string okumaDizini,
	               string dil)
{
	stdout.printf("Ana kütüğü ekliyorum\n");
	string kutukIsmi = dizineCevir(gecerliKutuk);
	string anaKutukIsmi = gecerliDizin + "/" + gecerliKutuk;

	var anaKutukAdi = File.new_for_path(anaKutukIsmi);
	stdout.printf("Ana kütük %s\n", anaKutukIsmi);
	if (anaKutukAdi.query_exists())
	{
		stdout.printf("Kütük adını yazıyorum\n");
		if (dil == "Arapça")
		{
			string komut = "echo " + anaKutukIsmi
				+ " > " + okumaDizini + "/." 
				+ kutukIsmi  + "/arapcaKutuk.txt";
		    stdout.printf("Komut %s", komut);
			Posix.system(komut);
			arapcaKutukEklendi = true;
		} 
       	else
		{
			string komut = "echo " + anaKutukIsmi
				+ " > " + okumaDizini + "/." 
				+ kutukIsmi  + "/turkceKutuk.txt";
		    stdout.printf("Komut %s", komut);
			Posix.system(komut);
			turkceKutukEklendi = true;
		}
	}
	kutukIsmiEklendi = true;

}

string hataliKelimeyiYaz(string kelime, string dil, string gecerliDizin)
{
	string degistir;
	string hataKutugu;
	stdout.printf("Buraya geldik\n");
	if (dil == "ara")
		hataKutugu = "arapcaHatali.txt";
	else
		hataKutugu = "turkceHatali.txt";

	string komut = "echo -e '" + kelime + "'"
		+ " >> " + gecerliDizin + "/" + hataKutugu;
	stdout.printf("Komut %s\n",komut);
	Posix.system(komut);
	komut = "echo -e '...'"
		+ " >> " + gecerliDizin  + "/" + hataKutugu;
	stdout.printf("Komut yeniden %s\n",komut);
	Posix.system(komut);
	degistir = kelime.replace("\n", " ");
	return degistir;
}

void kelimeEkle (string kelime, string gecerliDizin, string gecerliKutuk)
{
	string sonuc;
    string kutukIsmi = dizineCevir(gecerliKutuk);
    stdout.printf("Kütük ismi %s\n", kutukIsmi);
    string dizin = gecerliDizin + "/." + kutukIsmi;

    stdout.printf("Geçerli dizin %s\n", dizin);
    stdout.printf("Geçerli kütük %s\n", gecerliKutuk);


    var kelimeDizini = File.new_for_path(dizin);
    if (!kelimeDizini.query_exists())
    {
        stdout.printf("Dizini oluşturuyorum\n");
        Process.spawn_command_line_sync("mkdir " + dizin);
    }


	string arapcaKutuk = dizin +   "/kelimeler_ara.txt";
	string turkceKutuk = dizin + "/kelimeler_tur.txt";
    stdout.printf("Arapça kütük %s\n", arapcaKutuk);

    if (arapcaMi(kelime))
          sonuc = "ara";
    else
          sonuc = "tur";

    stdout.printf("%s\n", sonuc);

    if (sonuc == "ara")
	{
		string degistir = "";
		stdout.printf("Kelime arapça\n");
		stdout.printf("Kelime %s\n",kelime);
		if (kelime.contains("\n"))
		{
			degistir = hataliKelimeyiYaz(kelime, sonuc, dizin);
			kelime = degistir;
		}
		stdout.printf("Değiştirilen %s\n",degistir);
		stdout.printf("Kelime tekrardan %s\n",kelime);
		Posix.system("echo " + kelime + " >> " + arapcaKutuk);
	}
	else
	{
		string degistir = "";
		stdout.printf("Kelime arapça değil\n");
		stdout.printf("Kelime %s\n",kelime);
		if (kelime.contains("\n"))
		{
			degistir = hataliKelimeyiYaz(kelime, sonuc, dizin);
			kelime = degistir;
		}
		stdout.printf("Değiştirilen %s\n",degistir);
		stdout.printf("Kelime tekrardan %s\n",kelime);

		Posix.system("echo " + kelime + " >> " + turkceKutuk);

	}
}
