
                                                                                          /*
																						   *
   *                                                                                       *
 *   *  Kullanım : ./DugumYigit < tobe.txt                                                 *
   *                                                                                       *
        - Düğüm yığıt                                                                      *
                                                                                           */

class Yigit<Tur>
{
	Dugum<Tur> ilk;
	int elemanAdedi;

	class Dugum<Tur>
	{
		public Tur eleman;
		public Dugum<Tur> sonraki;
	}

	public Yigit()
	{
		ilk = null;
		elemanAdedi = 0;
	}

	public void ekle (Tur eleman)
	{
		Dugum<Tur> eskiIlk = ilk;
		ilk = new Dugum<Tur>();
		ilk.eleman = eleman;
		ilk.sonraki = eskiIlk;
		++elemanAdedi;
	}

	public Tur cikar()
	{
		Tur cikarilan = ilk.eleman;
		ilk = ilk.sonraki;
		--elemanAdedi;
		return cikarilan;
	}

	public bool bosMu()
	{
		return ilk == null;
	}

	public int elemanSayisi()
	{
		return elemanAdedi;
	}
}

// int main()
// {
// 	Yigit<string> yigit = new Yigit<string>();
// 	while (!stdin.eof())
// 	{
// 		var okunan = stdin.read_line();
// 		if (okunan == null)
// 		{
// 			stdout.printf("\n");
// 			return 0;
// 		}
// 		string[] kelimeler = okunan.split(" ");

// 		foreach (var kelime in kelimeler)
// 		{
// 			if (kelime == "-")
// 				stdout.printf(yigit.cikar() + " ");
// 			else
// 				yigit.ekle(kelime);
// 		}
// 	}
// 	return 0;
// }