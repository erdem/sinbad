static int sayac = 1;
string kucukHarfeCevir (string cevrilecek)
{
	string sonuc = "";

	unichar k;

	for (int i = 0; cevrilecek.get_next_char(ref i, out k);)
	{
		if (k.to_string() == "I")
			sonuc += "ı";
		else
		{
			k = k.tolower();
			sonuc += k.to_string();
		}
	}
	return sonuc;
}

void kaydet(string[] arapcaKelimeler, string[] turkceKelimeler)
{
	string kullanici = Environment.get_variable("KULLANICI");
	string sifre = Environment.get_variable("SIFRE");
	string sunucuAdresi = "mongodb+srv://"  + kullanici + ":" + sifre
		+ "@veritabani.yxxfge9.mongodb.net"
		+ "/?retryWrites=true&w=majority";
	//string sunucuAdresi = "mongodb://localhost:27017";
    Mongo.BsonError hata = Mongo.BsonError ();
	Mongo.init();
	Mongo.Uri adres = new Mongo.Uri.with_error (sunucuAdresi, hata);
	var yerel = new Mongo.Client.from_uri(adres);
	Mongo.Database veritabani = yerel.get_database("sözlük");
	Mongo.Collection kullanicilar = yerel.get_collection("sözlük",
														 "arapçatürkçe");

	for (int i = 0; i < arapcaKelimeler.length; ++i)
	{
		Mongo.Bson gonderi = new Mongo.Bson();
		gonderi.append_int32 ("_id", -1, sayac);
		gonderi.append_utf8 ("arapça", -1, arapcaKelimeler[i], -1);
		gonderi.append_utf8 ("türkçe", -1, turkceKelimeler[i], -1);
		gonderi.append_now_utc("ekleme", -1);
		gonderi.append_now_utc("güncelleme", -1);

		kullanicilar.insert_one(gonderi, null, null, hata);
		sayac = sayac + 1;
	}
}

string[] kutukOku(string kutukIsmi, bool yoksay = false)
{
	string[] kelimeler = {};
	var kutuk = File.new_for_path(kutukIsmi);
	var okunan = new DataInputStream (kutuk.read());
	string satir = "";
	string [] yoksayilanlar = { ".", "..", ".kutuklistesi.txt" };
	while ((satir = okunan.read_line(null)) != null)
	{
		if (yoksay)
		{
			if (!(satir.strip() in yoksayilanlar))
				kelimeler += satir.strip();
		}
		else
		{
			kelimeler += satir.strip();
		}
	}
	okunan.close();
	return kelimeler;
}

string[] gecerliDizinleriGetir()
{
	string okumaDizini = "/home/erdem/Belgeler/okuma";
	string[] gecerliDizinler = {};
	string komut = "ls -a | grep '^\\.'";
	Posix.system("(cd " + okumaDizini + " "+
				 "&& " + komut + " > .kutuklistesi.txt )");
	var kutukListesi = File.new_for_path(okumaDizini +
										 "/" + ".kutuklistesi.txt");
	if (kutukListesi.query_exists())
	{
		bool yoksay = true;
		string [] satirlar = kutukOku(okumaDizini
									  + "/" + ".kutuklistesi.txt"
									  , yoksay);

		foreach (var satir in satirlar)
		{
			var arapcaKutuk = File.new_for_path(okumaDizini
    								   + "/" + satir.strip() + "/" 
									   + "kelimeler_ara.txt");
			if (arapcaKutuk.query_exists())
			{
				gecerliDizinler += satir.strip();
			}
		}
	}
	return gecerliDizinler;
}

int main()
{
	string okumaDizini = "/home/erdem/Belgeler/okuma/";
	string[] arapcaKelimeler = {};
	string[] turkceKelimeler = {};
	string[] dizinler = gecerliDizinleriGetir();

	foreach (var dizin in dizinler)
	{
		string arapcaKutuk = okumaDizini + dizin + "/kelimeler_ara.txt";
		string turkceKutuk = okumaDizini + dizin + "/kelimeler_tur.txt";
		stdout.printf("Dizin %s\n", dizin[1:]);
		stdout.printf("Dizin %s\n",dizin);
		stdout.printf("Arapça kütük %s\n",arapcaKutuk);
		stdout.printf("Türkçe kütük %s\n",turkceKutuk);
		arapcaKelimeler = kutukOku(arapcaKutuk);
		turkceKelimeler = kutukOku(turkceKutuk);

		for (int i = 0; i < arapcaKelimeler.length; ++i)
		{
			turkceKelimeler[i] = kucukHarfeCevir(turkceKelimeler[i]);
			stdout.printf("Kelimeler %s %s\n",
						  arapcaKelimeler[i],
						  kucukHarfeCevir(turkceKelimeler[i]));
		}

		kaydet(arapcaKelimeler, turkceKelimeler);
	}

	Mongo.cleanup();

	return 0;
}

