using Gee;

bool kutukIsmiEklendi;
bool arapcaKutukEklendi;
bool turkceKutukEklendi;

class Pencere: Gtk.ApplicationWindow
{
	GLib.File? kutuk;
	Gtk.SourceFile anaKutuk;
	Gtk.SourceBuffer bellek;
	Gtk.SourceView tahta;
	Gtk.Clipboard notTahtasi;
	Gdk.Display ekran;
	Gtk.ScrolledWindow kayanPencere;
	Gtk.TextMark yaziKonumu;
	Yigit<string> sonuc;
	Yigit<string> sonGezilenler;
	HashMultiMap<string,string> sozluk;
	string bellektekiKelime;
	string oncekiKelime;
	string seciliKelime;
    string gecerliDizin;
    string gecerliKutuk;
	string gecerliDil;
	string okumaDizini;
	string acilacakKutuk;
	string evDizini;
	Trie kelimeAgaci;
	uint ciftMi;
	uint yaziTipiSayaci;
	uint renkSayaci;

	const ActionEntry[] eylemler = {
		{ "yeni", yeniBakalim } ,
		{ "ac", acBakalim } ,
		{ "kaydet", kaydetBakalim },
		{ "gecis-yap", gecisYapalim },
		{ "olarak-kaydet", olarakKaydetBakalim }
	};

	public Pencere (Uygulama uygulama, string acilacak)
	{
		Object (application: uygulama, title: "Sinbad yazı uygulaması");
		set_default_size(800, 600);
		this.acilacakKutuk = acilacak;

		this.key_press_event.connect (tusaBasildi);
		this.add_action_entries (eylemler, this);

		bellek = new Gtk.SourceBuffer (null);


		ekran = this.get_display();
		notTahtasi = Gtk.Clipboard.get_for_display(ekran,
									Gdk.SELECTION_CLIPBOARD);
		seciliKelime = notTahtasi.wait_for_text ();

        /*
		var renkYoneticisi = new Gtk.SourceStyleSchemeManager();
		var renk = renkYoneticisi.get_scheme("cobalt");

		bellek.set_style_scheme(renk);
        */

		tahta = new Gtk.SourceView.with_buffer (bellek);
		yaziKonumu = new Gtk.TextMark("corba", false);

		evDizini = evDizininiOku();
		stdout.printf("Ev dizinmizi %s\n", evDizini);

		sonuc = new Yigit<string>();
        gecerliDizin = "";
        gecerliKutuk = "";
		gecerliDil = "";
		okumaDizini =
		 kutuktenTekDegerOku(evDizini + "/.sinbad/okumaDizini.txt");
		kutukIsmiEklendi = false;
		arapcaKutukEklendi = false;
		turkceKutukEklendi = false;
		sonGezilenler = new Yigit<string>();
		sonGezilenleriGetir();

		sozluk = sozluguDoldur();
		kelimeAgaci = new Trie();

		var anahtarlar = sozluk.get_keys();

		foreach (var anahtar in anahtarlar)
		{
			kelimeAgaci.add(anahtar);
		}


		ciftMi = 0;

		yaziTipiSayaci = 1;
		renkSayaci = 0;

		var yaziTipi = new Pango.FontDescription();
		yaziTipi.set_family("ScheherazadeNew");
		//yaziTipi.set_family("HusrevHattiArapca");
		yaziTipi.set_size(22000);
		tahta.modify_font(yaziTipi);
		//tahta.buffer.text = "كيف حالك؟\nما اسمك؟\n";

		tahta.set_wrap_mode (Gtk.WrapMode.WORD);
		tahta.set_indent_width(4);
        tahta.set_tab_width(4);
		/*
		tahta.set_right_margin_position(75);
		tahta.set_show_right_margin(true);
		*/

		kayanPencere = new Gtk.ScrolledWindow (null, null);

		kayanPencere.add (tahta);


		this.add (kayanPencere);

		show_all();
	}

	~Pencere()
	{
		this.cikiyorum();
	}

	string evDizininiOku()
	{
		string okunan;
		Process.spawn_command_line_sync("sh -c 'echo $HOME'",
										 out okunan);
		return okunan.strip();
	}

	void sonGezilenleriKaydet()
	{
		var cikis =
			File.new_for_path(evDizini + "/.sinbad/sonAcilan.txt");
		if (cikis.query_exists ())
		{
            cikis.delete ();
        }

		var yazilan = new DataOutputStream
            (cikis.create (FileCreateFlags.REPLACE_DESTINATION));

		while (sonGezilenler.bosMu() == false)
		{
			yazilan.put_string(sonGezilenler.cikar() + "\n");
		}

	}

	public void cikiyorum()
	{
		sonGezilenleriKaydet();
	}

	private void sonGezileniAc(string acilacakKutuk)
	{
		uint8[] icerik;
		kutuk = File.new_for_path(acilacakKutuk);
		kutuk.load_contents(null, out icerik, null);

		gecerliKutuk = kutukIsminiGetir(acilacakKutuk);
		gecerliDizin = dizinYap(acilacakKutuk);

		string bakilacak = bakilacakOrnegiOlustur((string) icerik);

		if (arapcaMi(bakilacak))
		{
			gecerliDil = "ara";
		}
		else
		{
			gecerliDil = "tur";

		}
		// gecerliDil = diliBelirle("كيف

		string[] hatalilar = {};
		string kutukIcerigi = (string) icerik;
		string duzeltilmis = "";

		if (ceviriVarMi(gecerliKutuk))
		{
			if (hataliVarMi(gecerliKutuk,
							gecerliDil,
							gecerliDizin))
			{
				hatalilar = hatalilariGetir(gecerliKutuk,
											gecerliDil,
											gecerliDizin);
				foreach (var hatali in hatalilar)
				{
					duzeltilmis = hatali.replace("\n", " ");
					duzeltilmis = duzeltilmis.strip();
					kutukIcerigi = kutukIcerigi.replace(hatali,
														duzeltilmis);
				}
			}
		}


		string[] kelimeler = {};

		bellek.set_text (kutukIcerigi, kutukIcerigi.length);

		if (ceviriVarMi(gecerliKutuk))
		{
			kelimeler = kelimeleriGetir(gecerliKutuk,
										gecerliDil,
										gecerliDizin);
			uint renkSayaci = 0;
			foreach(var kelime in kelimeler)
			{
				renklendir(kelime, renkSayaci);
				renkSayaci += 1;
			}
		}

	}

    string kutukIsminiGetir (string uzunKutukIsmi)
    {
        string[] parcalar = uzunKutukIsmi.split("/");
        string kutukIsmi = "";

        foreach (unowned string parca in parcalar)
        {
            kutukIsmi = parca;
        }
        return kutukIsmi;
    }

	void sonGezilenleriGetir()
	{
		var kutuk =
			File.new_for_path(evDizini + "/.sinbad/sonAcilan.txt");
		var okunan = new DataInputStream (kutuk.read());

		string satir;

		anaKutuk = new Gtk.SourceFile ();


		while ((satir = okunan.read_line(null)) != null)
		{
            gecerliKutuk = kutukIsminiGetir(satir);
            gecerliDizin = dizinYap(satir);

			if (acilacakKutuk == "yok")
			 {
				sonGezileniAc(satir);
				sonGezilenler.ekle(satir);
			}
			else
			{
				sonGezileniAc(acilacakKutuk);
				sonGezilenler.ekle(acilacakKutuk);
			}


		}
	}

	Yigit<string> eslesenler (HashMultiMap<string,string> sozluk,
                              string girilenKelime)
	{
		Yigit<string> eslesenler = new Yigit<string>();
		Set<string> sonuc;

		if (girilenKelime.length > 1)
			kelimeAgaci.add(girilenKelime);

		Matcher islev = (girilen) =>
		{
			long uzunluk = long.min(girilen.length, 3);
			return girilen.substring(0, uzunluk) ==
                                bellektekiKelime.substring(0, uzunluk);
		};

		if (sozluk.contains(girilenKelime))
		{
			eslesenler.ekle(bellektekiKelime);
			foreach (var eleman in sozluk.get(girilenKelime))
			{
				if (eleman.has_prefix(bellektekiKelime))
					eslesenler.ekle(eleman);
			}

			if (eslesenler.elemanSayisi() == 1)
			{
				sonuc = kelimeAgaci.match(bellektekiKelime, islev);
				foreach (var anahtar in sonuc)
				{
					eslesenler.ekle(anahtar);
				}
			}
		}

		else
		{
			sonuc = kelimeAgaci.match(bellektekiKelime, islev);
			foreach (var anahtar in sonuc)
			{
				eslesenler.ekle(anahtar);
			}
		}

		return eslesenler;
	}

	private bool tusaBasildi (Gdk.EventKey tus)
	{
		Gtk.TextIter baslangic, bitis, oncekiBaslangic, oncekiBitis;
		string tusIsmi = Gdk.keyval_name(tus.keyval);
		int tusNumarasi = (int) tus.keyval;
		int ekTus = (int) tus.state;
		seciliKelime = notTahtasi.wait_for_text ();


		stdout.printf("Tuş ismi%s\n",tusIsmi);
		stdout.printf("Basılan tuş%d\n",tusNumarasi);
		stdout.printf("Ek tuş%d\n", ekTus);


		if ((tusNumarasi == 106) && (ekTus == 8) ||
			(tusNumarasi == 106) && (ekTus == 24) ||
			(tusNumarasi == 106) && (ekTus == 33554440) ||
			(tusNumarasi == 106) && (ekTus == 33554456))

		{
			stdout.printf("Buraya geldik mi\n");
			Gtk.TextBuffer yazilar = (Gtk.TextBuffer) tahta.get_buffer();
			yazilar.copy_clipboard(notTahtasi);
			seciliKelime = notTahtasi.wait_for_text ();

			Gtk.TextIter bas;
			Gtk.TextIter son;
			Gtk.TextIter neredeyiz;
			yazilar.get_selection_bounds(out bas, out son);
			yazilar.get_end_iter(out neredeyiz);
			kelimeEkle(seciliKelime, gecerliDizin, gecerliKutuk);

			if (arapcaMi(seciliKelime))
			{
				stdout.printf("Kelime arapçadır\n");
				if (!arapcaKutukEklendi)
				{
					anaKutuguEkle(seciliKelime, gecerliDizin,
								  gecerliKutuk, okumaDizini, "Arapça");
				}
			}
			else
			{
				stdout.printf("Kelime arapça değildir\n");
				if (!turkceKutukEklendi)
				{
					anaKutuguEkle(seciliKelime, gecerliDizin,
								  gecerliKutuk, okumaDizini, "Türkçe");

				}
			}

			// if (!kutukIsmiEklendi)
    		// {
			// 	stdout.printf("Kütük ismini ekleyebiliyor muyuz?\n");
			// 	anaKutuguEkle(seciliKelime, gecerliDizin,
			// 				  gecerliKutuk, okumaDizini);
			// }
			// else
			// {
			// 	stdout.printf("Kütük ismini ekleyemiyoruz\n");
			// }

			yazilar.delete(ref bas, ref son);
			string [] renkler = {"#3584E4", "#33D17A",
                                 "#E5A50A", "#FF7800",
                                 "#E01B24", "#9141AC", "#986A44"};
			uint secilenRenk = renkSayaci % renkler.length;
			seciliKelime = "<span foreground='"
			               + renkler[secilenRenk] + "'>"
			               + seciliKelime + "</span>";

			yazilar.insert_markup(ref bas, seciliKelime, -1);
			renkSayaci += 1;
		}


		if ((tus.keyval == 99) && (tus.state == 20))
		{
			bellek = (Gtk.SourceBuffer) tahta.get_buffer();
			bellek.copy_clipboard(notTahtasi);
		}

		if ((tus.keyval == 1484) && (tus.state == 8212))
		{
			bellek = (Gtk.SourceBuffer) tahta.get_buffer();
			bellek.copy_clipboard(notTahtasi);
		}


		if (tus.keyval == 65289)
		{

			this.title = "Basıldı";
			bellek = (Gtk.SourceBuffer) tahta.get_buffer();
			yaziKonumu = bellek.get_insert();

			bellek.get_iter_at_mark(out baslangic, yaziKonumu);
			if (baslangic.ends_word())
			{
				bitis = baslangic;
				baslangic.backward_word_start();
				string girilenKelime = bellek.get_text(baslangic,
													   bitis, false);
				bellektekiKelime = girilenKelime;

				oncekiBaslangic = baslangic;
				oncekiBitis = baslangic;

				oncekiBaslangic.backward_word_start();
				oncekiKelime = bellek.get_text(oncekiBaslangic,
											   oncekiBitis, false);
				oncekiKelime = oncekiKelime.split(" ")[0];


				if (sonuc.bosMu())
				{
					if (oncekiKelime != null)
						sonuc = eslesenler(sozluk, oncekiKelime);


					if (sonuc.elemanSayisi() > 0)
					{
						bellek.@delete(ref baslangic,ref bitis);
						bellek.insert(ref baslangic, sonuc.cikar(), -1);
					}
				}

				else
				{
					bellek.@delete(ref baslangic,ref bitis);

					bellek.insert(ref baslangic, sonuc.cikar(), -1);
				}

			}

			return true;

		}

		if (tus.keyval < 65000)
		{
			while (sonuc.bosMu() == false)
			{
				sonuc.cikar();
			}

			bellektekiKelime = "";
		}

		if (tus.keyval < 65288)
		{
			while (sonuc.bosMu() == false)
			{
				sonuc.cikar();
			}
			bellektekiKelime = "";

		}


		if (tus.keyval == 32)
		{
			while (sonuc.bosMu() == false)
			{
				sonuc.cikar();
			}

			bellektekiKelime = "";
		}
		return false;
	}


	string dizinYap (string kutukIsmi)
	{
		string dizin = "";
		string[] kelimeler = kutukIsmi.split("/");
		for (int i = 0; i < kelimeler.length - 1; ++i)
		{
			if (kelimeler[i] != "")
			{
				dizin += "/";
				dizin += kelimeler[i];
			}
		}
		return dizin;
	}

	void gecisYapalim()
	{
		++yaziTipiSayaci;

		if (yaziTipiSayaci % 2 == 0)
		{
			var yaziTipi = new Pango.FontDescription();
			yaziTipi.set_family("Monospace");
			yaziTipi.set_size(13000);
			tahta.modify_font(yaziTipi);
		}
		else
		{
			var yaziTipi = new Pango.FontDescription();
			yaziTipi.set_family("ScheherazadeNew");
			yaziTipi.set_size(22000);
			tahta.modify_font(yaziTipi);

		}

	}



	void yeniBakalim (SimpleAction eylem, Variant? birseyler)
	{
		kutuk = null;
		bellek.set_text ("");
	}

	string kutuktenTekDegerOku (string kutukIsmi)
	{
		return kutukOku(kutukIsmi)[0].strip();
	}

	string[] kutukOku(string kutukIsmi)
	{
		string[] kelimeler = {};
		var kutuk = File.new_for_path(kutukIsmi);
		var okunan = new DataInputStream (kutuk.read());
		string satir = "";
		while ((satir = okunan.read_line(null)) != null)
		{
			kelimeler += satir.strip();
		}
		return kelimeler;
	}

	string[] hatalilariOku (string kutukIsmi)
	{
		string[] hatalilar = {};
		string hatali = "";
		string satir = "";
		var kutuk = File.new_for_path(kutukIsmi);
		var okunan = new DataInputStream(kutuk.read());
		while ((satir = okunan.read_line(null)) != null)
		{
			if (satir.strip() == "...")
			{
				hatalilar += hatali.strip();
				hatali = "";
			}
			else
			{
				hatali += satir + "\n";
			}
		}
		return hatalilar;
	}

	bool hataliVarMi(string gecerliKutuk,
					 string gecerliDil,
					 string gecerliDizin)
	{
		string kisaDizin = dizineCevir(gecerliKutuk);
		string dizin = gecerliDizin + "/." + kisaDizin;
		string arapcaHatali = dizin +   "/arapcaHatali.txt";
		string turkceHatali = dizin + "/turkceHatali.txt";

		var arapcaHataliKutuk = File.new_for_path(arapcaHatali);
		if (arapcaHataliKutuk.query_exists())
			return true;
		var turkceHataliKutuk = File.new_for_path(turkceHatali);
		if (turkceHataliKutuk.query_exists())
			return true;

		return false;
	}

	string[] hatalilariGetir(string gecerliKutuk,
							 string gecerliDil,
							 string gecerliDizin)
	{
		string[] hatalilar = {};
		string kisaDizin = dizineCevir(gecerliKutuk);
		string dizin = gecerliDizin + "/." + kisaDizin;
		string arapcaHatali = dizin +   "/arapcaHatali.txt";
		string turkceHatali = dizin + "/turkceHatali.txt";

		if (gecerliDil == "ara")
		{
			hatalilar = hatalilariOku(arapcaHatali);
		}
		else
		{
			hatalilar = hatalilariOku(turkceHatali);
		}

		return hatalilar;
	}

	string[] kelimeleriGetir(string gecerliKutuk,
                             string gecerliDil,
		                     string gecerliDizin)
	{
		string[] kelimeler = {};
		string kisaDizin = dizineCevir(gecerliKutuk);
		string dizin = gecerliDizin + "/." + kisaDizin;
		string arapcaKutuk = dizin +   "/kelimeler_ara.txt";
		string turkceKutuk = dizin + "/kelimeler_tur.txt";


		if (gecerliDil == "ara")
		{
			kelimeler = kutukOku(arapcaKutuk);
		}
		else
		{
			kelimeler = kutukOku(turkceKutuk);
		}
		return kelimeler;

	}

    bool ceviriVarMi(string gecerli)
    {
		string dizin = okumaDizini + "/" + "." + dizineCevir(gecerli);
		var aranan = File.new_for_path(dizin);
		if (aranan.query_exists())
		{
			return true;
		}
		else
		{
			return false;
		}
    }

	void acBakalim (SimpleAction eylem, Variant? birseyler)
	{
		var dosyaAc =
            new Gtk.FileChooserDialog ("Dosya seçiniz",
                                       this as Gtk.Window,
                                       Gtk.FileChooserAction.OPEN,
                                       Gtk.Stock.CANCEL,
                                       Gtk.ResponseType.CANCEL,
                                       Gtk.Stock.OPEN,
                                       Gtk.ResponseType.ACCEPT);
		string sonGezilen;

		if (!sonGezilenler.bosMu())
			sonGezilen = sonGezilenler.cikar();
		else
			sonGezilen = "";
		if (sonGezilen != "")
		{
			string dizin = dizinYap(sonGezilen);
            gecerliDizin = dizin;
			dosyaAc.set_current_folder(dizin);
		}
		else
			stdout.printf("Son gezilenler boş\n");


		dosyaAc.local_only = false;
		dosyaAc.set_modal (true);
		dosyaAc.response.connect (acilanYanitVer);
		dosyaAc.show ();
	}

	bool arapcaMi (string metin)
	{
		string alfabe = "ابتثجحخدذرزسشصضعغفقكلمنوهي";
		for (int i=0; i < metin.char_count(); i++)
		{
			string karakter =
			metin.get_char(metin.index_of_nth_char(i)).to_string();
			if (alfabe.contains(karakter))
			{
				return true;
			}
		}
		return false;
	}

	void renklendir (string kelime, uint renkSayaci)
	{
		string [] renkler = {"#3584E4", "#33D17A",
                             "#E5A50A", "#FF7800",
                             "#E01B24", "#9141AC", "#986A44"};
		uint secilenRenk = renkSayaci % renkler.length;

	    // tahta SourceView - TextView
        // bellek SourceBuffer - TextBuffer
        Gtk.TextIter bas;
        Gtk.TextIter son;
        Gtk.TextIter aramabas;
        Gtk.TextIter aramason;


        bellek.get_start_iter(out bas);
        bellek.get_end_iter(out son);

        bool sonuc = bas.forward_search(kelime,
                     Gtk.TextSearchFlags.VISIBLE_ONLY,
                     out aramabas,
                     out aramason,
                     null);
        if (sonuc)
		{
            bellek.delete(ref aramabas, ref aramason);
			string yeniKelime = "<span foreground='"
			                    + renkler[secilenRenk] + "'>"
			                    + kelime + "</span>";
            bellek.insert_markup(ref aramabas, yeniKelime, -1);

        }

	}

	string bakilacakOrnegiOlustur (string uzun)
	{
		string sonuc = "";

		for (int i=0; i < 19; i++)
		{
			string karakter =
			uzun.get_char(uzun.index_of_nth_char(i)).to_string();
		    sonuc += karakter;
		}
		return sonuc;
	}


	void acilanYanitVer (Gtk.Dialog pencere, int yanitNumarasi)
	{
		var acPenceresi = pencere as Gtk.FileChooserDialog;
		string sonAcilan;

		switch (yanitNumarasi)
		{
		case Gtk.ResponseType.ACCEPT:
		{
			kutukIsmiEklendi = false;
			kutuk = acPenceresi.get_file();
			sonAcilan = acPenceresi.get_filename();
			//sonGezilenler.ekle(sonAcilan);

			uint8[] icerik;
			try
			{
				kutuk.load_contents(null, out icerik, null);
			}
			catch (GLib.Error hata)
			{
				error ("%s\n", hata.message);
			}

			gecerliKutuk = kutukIsminiGetir(sonAcilan);
            gecerliDizin = dizinYap(sonAcilan);
			string bakilacak = bakilacakOrnegiOlustur((string) icerik);


			if (arapcaMi(bakilacak))
			{
				gecerliDil = "ara";
			}
			else
			{
				gecerliDil = "tur";

			}

			string[] hatalilar = {};
			string kutukIcerigi = (string) icerik;
			string duzeltilmis = "";

			if (ceviriVarMi(gecerliKutuk))
			{
				if (hataliVarMi(gecerliKutuk,
								gecerliDil,
								gecerliDizin))
				{
					hatalilar = hatalilariGetir(gecerliKutuk,
												gecerliDil,
												gecerliDizin);
					foreach (var hatali in hatalilar)
					{
						duzeltilmis = hatali.replace("\n", " ");
						duzeltilmis = duzeltilmis.strip();
						kutukIcerigi = kutukIcerigi.replace(hatali,
															duzeltilmis);
					 }
				}
			}

			string[] kelimeler = {};

			bellek.set_text (kutukIcerigi, kutukIcerigi.length);

            if (ceviriVarMi(gecerliKutuk))
			{
				kelimeler = kelimeleriGetir(gecerliKutuk,
					                        gecerliDil,
 					                        gecerliDizin);
				uint renkSayaci = 0;
			    foreach(var kelime in kelimeler)
				{
					renklendir(kelime, renkSayaci);
				    renkSayaci += 1;
				}
			}

			sonGezilenler.ekle(sonAcilan);

			string dizin = acPenceresi.get_current_folder();
			var yonetici = new Gtk.SourceLanguageManager();

			FileInfo kutukBilgisi = kutuk.query_info("standard::*",
                                                     0, null);
			string dosyaTuru = kutukBilgisi.get_content_type();
			string tur = GLib.ContentType.get_mime_type(dosyaTuru);
			Gtk.SourceLanguage dil = yonetici.guess_language(dizin, tur);
			bellek.set_language(dil);

			break;
		}
		case Gtk.ResponseType.CANCEL:
			break;
		}
		pencere.destroy();
	}

	void olarakKaydetYanitVer (Gtk.Dialog pencere, int yanitNumarasi)
	{
		var kaydetPenceresi = pencere as Gtk.FileChooserDialog;

		switch (yanitNumarasi)
		{
		case Gtk.ResponseType.ACCEPT:
			kutuk = kaydetPenceresi.get_file();
			this.kutugeKaydet();
			break;
		default:
			break;
		}
		pencere.destroy();
	}

	void olarakKaydetBakalim(SimpleAction eylem, Variant? birseyler)
	{

		var kaydetPenceresi =
		new Gtk.FileChooserDialog ("Kütük seçiniz",
								   this as Gtk.Window,
								   Gtk.FileChooserAction.SAVE,
								   Gtk.Stock.CANCEL,
								   Gtk.ResponseType.CANCEL,
								   Gtk.Stock.SAVE,
								   Gtk.ResponseType.ACCEPT);

		kaydetPenceresi.set_do_overwrite_confirmation (true);
		kaydetPenceresi.set_modal (true);

		if (kutuk != null)
		{
			try
			{
				(kaydetPenceresi as Gtk.FileChooser).set_file(kutuk);
			}
			catch (GLib.Error hata)
			{
				print ("%s\n", hata.message);
			}
		}
		kaydetPenceresi.response.connect (olarakKaydetYanitVer);
		kaydetPenceresi.show();
	}


	void kaydetBakalim (SimpleAction eylem, Variant? birseyler)
	{
		if (kutuk != null)
		{
			this.kutugeKaydet();
		}
		else
		{
			olarakKaydetBakalim(eylem,  birseyler);
		}
	}

	void kutugeKaydet()
	{
		Gtk.TextIter baslangic;
		Gtk.TextIter bitis;

		bellek.get_bounds (out baslangic, out bitis);
		string halihazirdakiIcerik =
            bellek.get_text (baslangic, bitis, false);

		try
		{
			kutuk.replace_contents (halihazirdakiIcerik.data,
                                    null, false,
									GLib.FileCreateFlags.NONE,
                                    null, null);
		}
		catch (GLib.Error hata)
		{
			error ("%s\n", hata.message);
		}

	}


}


public  class Uygulama: Gtk.Application
{
	Pencere pencere;

	private void pencereOlustur(string kutukIsmi)
	{
		pencere = new Pencere(this, kutukIsmi);
		pencere.show_all();
	}

	private int komutSatiriniKullan (ApplicationCommandLine komutSatiri)
	{
		string[] corbalar = komutSatiri.get_arguments();
		if (corbalar[1] == null)
			this.activate();
		else
			pencereOlustur(corbalar[1]);
		return 0;
	}

	public override int command_line (ApplicationCommandLine komutSatiri)
	{
		int sonuc = komutSatiriniKullan (komutSatiri);
		return sonuc;
	}

	protected override void activate()
	{
		pencere = new Pencere(this, "yok");
		pencere.show_all();
	}

	const ActionEntry[] eylemler = {
		{ "cikis", cikalimBakalim }
	};


	void cikalimBakalim (SimpleAction eylem, Variant? birsey)
	{
		this.pencere.cikiyorum();
		this.quit();
	}

	string evDizininiOku()
	{
		string okunan;
		Process.spawn_command_line_sync("sh -c 'echo $HOME'",
										 out okunan);
		return okunan.strip();
	}


	protected override void startup()
	{
		base.startup();

		/* Eylemleri ekle */
		this.add_action_entries (eylemler, this);

		var menuOlusturucu = new Gtk.Builder();
		try
		{
			string evDizini = evDizininiOku();
			stdout.printf("Ev dizini %s\n", evDizini + "/.sinbad");
			menuOlusturucu.add_from_file( evDizini +
										  "/.sinbad/sinbad.ui");
		}
		catch (GLib.Error hata)
		{
			error ("Kütüğü yükleyemedim: %s\n", hata.message);
		}

		this.app_menu =
            menuOlusturucu.get_object("appmenu") as MenuModel;
	}

	public Uygulama()
	{
		Object (application_id: "erdem.vala.sinbad",
				flags: ApplicationFlags.HANDLES_COMMAND_LINE);
	}
}

int main (string[] corbalar)
{
	Gtk.init (ref corbalar);
	var uygulama = new Uygulama();
	return uygulama.run(corbalar);
}