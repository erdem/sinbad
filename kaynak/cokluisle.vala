using Gee;
using Posix;

string[] kutukOku(string kutukIsmi)
{
	string[] kelimeler = {};
	var kutuk = File.new_for_path(kutukIsmi);
	var okunan = new DataInputStream (kutuk.read());
	string satir = "";
	while ((satir = okunan.read_line(null)) != null)
	{
		kelimeler += satir.strip();
	}
	return kelimeler;
}


bool kutukIsmiGecerliMi (string kutukIsmi)
{
	if (FileUtils.test (kutukIsmi, FileTest.IS_DIR) == true)
		return false;
	if (kutukIsmi == "")
		return false;
	if (kutukIsmi.split("_")[1] == "tur.txt")
		return false;
	string uzanti = kutukIsmi.split(".")[1];
	if (uzanti == "pdf" ||
		uzanti == "html"||
		uzanti == "css" ||
		uzanti == "htm" ||
		uzanti == "DOC" ||
		uzanti == "doc" ||
		uzanti == "mp3" ||
		uzanti == "xopp"||
		uzanti == "xoj" ||
		uzanti == "ods" ||
		uzanti == "odt")
		return false;

	if (kutukIsmi[0] == '.')
		return false;
	for (int i = 0; i < kutukIsmi.length; ++i)
	{
		if (kutukIsmi[i] == '#' || kutukIsmi[i] == '~')
			return false;
	}
	return true;
}

bool dizinGecerliMi (string dizin)
{
	return FileUtils.test (dizin, FileTest.IS_DIR);
}

string evDizininiOku()
{
	string okunan;
	Process.spawn_command_line_sync("sh -c 'echo $HOME'",
									out okunan);
	return okunan.strip();
}


HashMultiMap<string, string> sozluguDoldur()
{

	string evDizini = evDizininiOku();
	string[] dizinListesi =
				 kutukOku(evDizini + "/.sinbad/kaynakDizini.txt");


	var sozluk = new HashMultiMap<string, string>();
	foreach (string dizin in dizinListesi)
	{
		if (dizinGecerliMi (dizin))
		{
			try
			{
				GLib.stdout.printf("Okuma dizinini açıyorum\n");
				GLib.stdout.printf("Okuma kaynak dizini: %s\n", dizin);
				chdir(dizin);
				//system("ls -l");
			}
			catch (Error hata)
			{
				error ("%s", hata.message);
			}



			string[] kutukListesi = new string[] { "" };

			try
			{
				var dizinimiz = File.new_for_path(dizin);
				string dizinIsmi = dizinimiz.get_path();
				var gezici = dizinimiz.enumerate_children
                    (FileAttribute.STANDARD_NAME, 0);
				FileInfo kutukBilgi = null;
				while ((kutukBilgi = gezici.next_file()) != null)
				{
					kutukListesi += kutukBilgi.get_name();
				}
				foreach (var isim in kutukListesi)
					GLib.stdout.printf("Kütük listesi %s", isim);
				//				GLib.stdout.printf("Kütük listesi %s", kutukListesi);
			}
			catch (Error hata)
			{
				error ("%s", hata.message);
			}


			foreach (var isim in kutukListesi)
			{
				if (kutukIsmiGecerliMi(isim))
					GLib.stdout.printf("Kütük ismi :%s:\n", isim);
				if (kutukIsmiGecerliMi(isim))
				{
					var kutuk = File.new_for_path(isim);

					try
					{
						var okunan = new DataInputStream (kutuk.read ());
						string satir;
						string ilkKelime = "";
						string ikinciKelime = "";

						while ((satir = okunan.read_line (null)) != null)
						{
							string[] kelimeler = satir.split(" ");

							foreach (var kelime in kelimeler)
							{

								string duzeltilmisKelime =
                                    kelimeDuzelt(kelime);

								if (duzeltilmisKelime.length > 1)
								{

									ilkKelime = ikinciKelime;

									ikinciKelime = duzeltilmisKelime;

									if (ilkKelime.length > 1)
									{
										// kontrol
										// GLib.stdout.printf ("%s : %s\n", ilkKelime, ikinciKelime);
										sozluk[ilkKelime] = ikinciKelime;
									}
								}

							}

						}
					}
					catch (Error hata)
					{
						error ("%s", hata.message);
					}
				}
			}
		}
	}

	return sozluk;

}

bool noktalamaVarMi = false;

string kelimeDuzelt (string kelime)
{
	if (kelime == "»")
		return "";
	if (kelime == "ء")
		return "";
	if (kelime == "ل")
		return "";
	if (kelime == "ا")
		return "";

	if (kelime.length > 1 )
	{
		//GLib.stdout.printf("Kelime %s\n", kelime);

		unichar harf;
		noktalamaVarMi = false;


		string duzeltilmisKelime = "";


		for (int i = 0; kelime.get_next_char(ref i, out harf);)
		{

			UnicodeType tur = harf.type ();
//		GLib.stdout.printf("Tür = %s\n", tur.to_string());
			if (tur == UnicodeType.OTHER_PUNCTUATION)
			{
//			GLib.stdout.printf("Noktalama olduğu için yazmamız gerekiyor\n");
				noktalamaVarMi = true;

			}
			else if (tur == UnicodeType.INITIAL_PUNCTUATION)
			{
				//     «
			}
			else if (tur == UnicodeType.FINAL_PUNCTUATION)
			{
				//     »
			}

			else if (tur == UnicodeType.DECIMAL_NUMBER)
			{
//			GLib.stdout.printf("Sayı olduğu için yazmamız gerekiyor\n");
			}
			else if (tur == UnicodeType.MODIFIER_LETTER)
			{
//			GLib.stdout.printf("Sayı olduğu için yazmamız gerekiyor\n");
			}

			else if (tur == UnicodeType.DASH_PUNCTUATION)
			{
//			GLib.stdout.printf("Sayı olduğu için yazmamız gerekiyor\n");
			}


			else if (tur == UnicodeType.OPEN_PUNCTUATION || tur == UnicodeType.CLOSE_PUNCTUATION)
			{
//			GLib.stdout.printf("Parantez işaretleri\n");
			}
			else
			{
//			GLib.stdout.printf("Harf %s\n", harf.to_string());
				duzeltilmisKelime += harf.to_string();
			}
		}


		return duzeltilmisKelime;
	}
	else
		return "";
}
