#!/bin/bash

echo "Kuruluma başlıyorum"

echo "Kullanıcı $USER"

mkdir -p ~/bin

DAGITIM=$(python3 -m platform)


if [[ $DAGITIM == *"FreeBSD"* ]]; then
    echo "FreeBSD kullanıyorsunuz"
    cp kaynak/sinbad_bsd ~/bin/sinbad
else
    echo "Başka bir dağıtım kullanıyorsunuz"
    cp kaynak/sinbad ~/bin/
fi

echo "Kullanıcı $USER"


KABUK=$(echo $SHELL)

echo "Kabuk $KABUK"


if [[ ":$PATH:" == *":$HOME/bin:"* ]]; then
  echo "Çalışıyor"
else
    echo "Eklemek gerekecek"
    if [[ $KABUK == *"fish"* ]]; then
        echo "Balık kabuğunu kullanıyorsunuz"
        echo '' >>  ~/.config/fish/config.fish
        echo 'export PATH="$HOME/bin:$PATH"' >> ~/.config/fish/config.fish
    fi
fi

mkdir -p ~/Belgeler/okuma

cp -R  veri/okuma/*.* ~/Belgeler/okuma
cp -R  veri/okuma/.ceylanvekurt ~/Belgeler/okuma

mkdir -p ~/.sinbad

cp veri/sinbad.ui ~/.sinbad

mkdir -p ~/.fonts
cd veri/
unzip -oqq yazıtipi.zip
cd yazi
cp ScheherazadeNew-Regular.ttf ~/.fonts
cp ScheherazadeNew-Bold.ttf ~/.fonts


cd ~/.sinbad

echo $HOME/Belgeler/okuma > kaynakDizini.txt
echo $HOME/Belgeler/okuma > okumaDizini.txt
echo $HOME/Belgeler/okuma/ceylanvekurt.txt > sonAcilan.txt

