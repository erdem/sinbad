# Sinbad

# Nedir ?

Sinbad, renkli çeviri özelliğinin yanında otomatik tamamlama
özelliği bulunan bir yazı uygulamasıdır.

# Kurulum

Tüm linux dağıtımlarında kurulum için uygulamayı çekin.

``` bash
git clone https://codeberg.org/erdem/sinbad
```

Dizinin içine giriniz.

``` bash
cd sinbad/
```

Paketi kurmak için aşağıdaki komutu verebilirsiniz.

``` bash
bash kur.sh
```

Paketi kaldırmak için aşağıdaki komutu verebilirsiniz.

``` bash
bash kaldır.sh
```

Eğer kurulumda bir hata yaşarsanız [yeni bir konu](https://codeberg.org/erdem/sinbad/issues) oluşturabilirsiniz.

# Kullanım

Uygulama her yerden

``` bash
sinbad
```
komutu ile çalıştırılabilir


# Özellikler

## Renkli çeviri

Yazılımı ilk başlattığınızda renkli bir ekran ile karşılaşıyor
olmanız lazım.

Bunu `işletim sistemi tuşu` ve sağ ok tuşuna basarak sağa taşıyınız.

Bir tane daha Sinbad uygulaması başlatınız.

`Aç` bölümünden `ceylanvekurt_turkce.txt` kütüğünü seçiniz.

Pencereyi işletim sistemi tuşu ve sol ok tuşu ile sola taşıyınız.

Aşağıdakine benzer bir ekran görüyor olmalısınız.

<div align="center">
<img src="https://elektronik.vercel.app/resim/sinbad6.png">
</div>

Kelimeyi fare ile seçip, Alt-j tuşları ile renklendirebilirsiniz.

Renkli çeviri özelliğini açık hale getirebilmek için
`~./sinbad` dizininde bulunan `okumaDizini.txt` kütüğüne
sadece bir tane dizin ekledik.

Belgeler içinde `okuma` isimli dizini ana çalışma dizini
olarak belirledik.

Arzu ederseniz bu kütüğü inceleyebilir, ya da dizini değiştirebilirsiniz.

Okuma dizini kütüğünde tek dizin olması gerekiyor.

Kaynak dizini birden çok olabilir.

## Otomatik tamamlama

Sinbad otomatik tamamlama özelliği bulunan bir yazı uygulamasıdır.
Arapça'nın yanında Türkçe otomatik tamamlama yapabilir.

Kaynak dizini de  `~/.sinbad` dizininde bulunan
`kaynakDizini.txt` kütüğünde Belgeler `okuma` olarak belirlenmiştir.

Bu kütüğe birden fazla dizin ekleyebilirsiniz.

Otomatik tamamlamayı Tab tuşu ile kullanabilirsiniz.

Bu özelliği denemek için aşağıdaki cümleyi yazmak istediğimizi
düşünelim.

```
Tam bir sürâtla kaçmaya çalışarak bacaklarını rüzgâra bıraktı.
```

şeklinde bir cümle olsun.

Sinbad ile bu yazıyı tekrar yazmak istediğimizi düşünelim.

Öncelikle `Uygulama` `Yeni` yolunu takip ederek boş bir kütük açınız.


```
Tam
```
yazdınız. Bu ilk kelime olduğu ve kendinden önce başka kelime
olmadığı için eğer varsa Tam ile başlayan tüm kelimelerden tamamlar.

```
Tam b
```

yazar ve Tab tuşuna basarsanız b harfini "bir" yapar.

```
Tam bir s
```
yazar ve Tab tuşuna basarsanız s harfini "sürâtla" yapar.

# Nasıl Arapça yazabiliriz?

Eğer Türkçe F klavye kullanıyorsanız yeni bir Arapça klavye satın almadan
da [Arapça yazabilirsiniz](https://elektronik.vercel.app/arapca-yereller-ve-klavye).


# Sınama

Aşağıdaki dağıtımlarda uygulamanın sorunsuz çalıştığı sınanmıştır.

## Sınama Tarihi - Dağıtım

Şubat 2024 - Archlinux <br />

Şubat 2024 - NomadBSD <br />

Nisan 2024 - ArcoLinux